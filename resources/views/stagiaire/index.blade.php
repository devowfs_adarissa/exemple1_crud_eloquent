<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Liste des stagiaires</title>
</head>
<body class="container">
    <h2 class="mt-3">Liste des stagiaires</h2>
     @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
    @isset($stagiaires)
        <table class="table mt-3">
            <tr>
                <th>Id</th>
                <th>Nom complet</th>
                <th>Genre</th>
                <th>Date de naissance</th>
                <th>Note</th>
                <th>Groupe</th>
                <th>Action</th>
            </tr>
            @foreach($stagiaires as $stagiaire)
                <tr>
                    <td>{{ $stagiaire->id }}</td>
                    <td>{{ $stagiaire->nom_complet }}</td>
                    <td>{{ $stagiaire->genre }}</td>
                    <td>{{  $stagiaire->date_naissance->format('d/m/Y') }}</td>
                    <td>{{ $stagiaire->note }}</td>
                    <td>{{ $stagiaire->groupe_id }}</td>
                    <td>
                        <form method="post" action="{{ route('stagiaire.destroy', $stagiaire->id) }}">
								@csrf
								@method('DELETE')
								<a href="{{ route('stagiaire.show', $stagiaire->id) }}" class="btn btn-primary btn-sm">Afficher</a>
								<a href="{{ route('stagiaire.edit', $stagiaire->id) }}" class="btn btn-warning btn-sm">Modifier</a>
								<input type="submit" class="btn btn-danger btn-sm" value="Supprimer" />
							</form>
                    </td>
                    <td></td>
                </tr>
            @endforeach
        </table>
    @endisset
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
</html>