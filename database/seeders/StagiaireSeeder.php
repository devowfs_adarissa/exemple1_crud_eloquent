<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StagiaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table("stagiaires")->insert([
            [
                "nom_complet"=>"Tazi Naoual",
                "date_naissance"=>"2002-03-02",
                "genre"=>"F",
                "note"=>12,
                "groupe_id"=>1,
            ],
             [
                "nom_complet"=>"Ali Alami",
                "date_naissance"=>"2001-12-06",
                "genre"=>"M",
                "note"=>15,
                "groupe_id"=>2,
            ],
             [
                "nom_complet"=>"Touati Sara",
                "date_naissance"=>"2003-06-10",
                "genre"=>"F",
                "note"=>16.5,
                "groupe_id"=>2
            ]

        ]);

    }
}
