<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table("groupes")->insert([
            [
                "libelle"=>"DEVOWFS201",
                "filiere_id"=>2
            ],
              [
                "libelle"=>"DEVOWFS202",
                "filiere_id"=>2
            ],
              [
                "libelle"=>"DEVOAM202",
                "filiere_id"=>3
            ]
        ]);
    }
}
