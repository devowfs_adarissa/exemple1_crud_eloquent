<?php

namespace App\Http\Controllers;

use App\Models\Stagiaire;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StagiaireController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $stagiaires=Stagiaire::all();
        return view("stagiaire.index", compact("stagiaires"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("stagiaire.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $stagiaire = Stagiaire::create([
            "nom_complet"=>$request->nom_complet,
            "genre"=>$request->genre,
            "date_naissance"=>$request->date_naissance,
            "note"=>$request->note,
            "groupe_id"=>$request->groupe_id,
        ]);
        
        return redirect()->route('stagiaire.create')->with('success','Le stagiaire a été ajouté avec succès.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Stagiaire $stagiaire)
    {
        return view("stagiaire.show", compact("stagiaire"));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Stagiaire $stagiaire)
    {
        return view("stagiaire.edit", compact("stagiaire"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Stagiaire $stagiaire)
    {
        
        // $stagiaire->nom_complet=$request->nom_complet;
        // $stagiaire->genre=$request->genre;
        // $stagiaire->date_naissance=$request->date_naissance;
        //  $stagiaire->note=$request->note;
        // $stagiaire->groupe_id=$request->groupe_id;
        //$stagiaire->save();

        $stagiaire->update($request->all());
        return redirect()->route('stagiaire.index')->with('success','Le stagiaire a été modifié avec succès.');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Stagiaire $stagiaire)
    {
        $stagiaire->delete();
         return redirect()->route('stagiaire.index')->with('success','Le stagiaire a été supprimé avec succès.');

    }
}
