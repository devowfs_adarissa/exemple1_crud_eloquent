<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stagiaire extends Model
{
    use HasFactory;
    protected $fillable= ["nom_complet","genre", "date_naissance", "note","groupe_id"];
    protected $casts=[
    'date_naissance' => 'datetime:Y-m-d',
        ];
}
